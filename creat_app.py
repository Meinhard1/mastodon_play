import requests
import json

# Set up the data for the app registration request
data = {
    'client_name': 'post scraper test',
    'redirect_uris': 'urn:ietf:wg:oauth:2.0:oob',
    'scopes': 'read write push',
    'website': 'https://myapp.example'
}

# Send the app registration request to the Mastodon API
response = requests.post('https://mastodon.social/api/v1/apps', data=data)

creds = response.json()
# Select the keywords you want to save
creds = {'client_id': creds['client_id'], 'client_secret': creds['client_secret'], 'vapid_key': creds['vapid_key']}

# Save the selected keywords to a JSON file
with open('creds.json', 'w') as outfile:
    json.dump(creds, outfile)

