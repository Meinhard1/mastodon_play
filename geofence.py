"""
Note, this is not working as expected
"""

import requests
import json

# get app creds
with open('creds.json', 'r') as infile:
    creds = json.load(infile)

# Set up authentication credentials
access_token = creds['client_id']
api_base_url = 'https://mastodon.social'

# Specify geolocation data for the search query
latitude = 49.2827
longitude = -123.1207
radius = 30 # in kilometers

# Set up search query
search_query = 'Vancouver'

# Send search request to Mastodon API
headers = {'Authorization': f'Bearer {access_token}'}
params = {'q': search_query, 'latitude': latitude, 'longitude': longitude, 'radius': radius}
response = requests.get(f'{api_base_url}/api/v2/search', headers=headers, params=params)

# Parse response data
search_results = json.loads(response.text)
for status in search_results['statuses']:
    print(status['content'])
