import requests
import json
import pandas as pd


"""
view hashtag query params 
https://docs.joinmastodon.org/methods/timelines/#tag
"""
# basic params
hashtags = 'housing affordability'
feed_limit = 2

# get data from endpoint
response = requests.get(f"https://mastodon.social/api/v1/timelines/tag/{hashtags}?limit={feed_limit}")
statuses = json.loads(response.text) # this converts the json to a python list of dictionary

# format data to a dataframe to read easier
df = pd.json_normalize(statuses)
